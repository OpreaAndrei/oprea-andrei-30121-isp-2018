package Oprea.Andrei.Lab2.ex6;

import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {
        int n, factorial = 1;
        Scanner in = new Scanner(System.in);
        System.out.print("introduceti numarul : ");
        n = in.nextInt();
        for (int i = 1; i <= n; i++) {

            factorial *= i;
        }
        System.out.println("factorialul este " + factorial);

    }
}
