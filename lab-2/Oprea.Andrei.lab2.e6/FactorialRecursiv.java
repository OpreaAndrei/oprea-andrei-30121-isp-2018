package Oprea.Andrei.Lab2.ex6;

import java.util.Scanner;

public class FactorialRecursiv {

    public static int fact(int n) {
        if (n > 1)
            return n * fact(n - 1);
        else
            return 1;
    }

    public static void main(String[] args) {


        int n, factorial = 1;
        Scanner in = new Scanner(System.in);
        System.out.print("introduceti numarul : ");
        n = in.nextInt();
        if (n < 0) {
            System.out.print("nu putem calcula factorialul");
            System.out.println();
            return;
        }
        if (n == 0) {
            System.out.print("Factorialul este 1");
            System.out.println();
            return;
        }
        if (n == 1) {
            System.out.print("Factorialul este 1");
            System.out.println();
            return;
        }

        if (n > 1)
            factorial = fact(n);


        System.out.print("Factorialul  este : " + factorial);
        System.out.println();
    }
}


