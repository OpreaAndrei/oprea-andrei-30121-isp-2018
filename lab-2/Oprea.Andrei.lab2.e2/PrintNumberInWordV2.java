package Lab2.ex2;
import java.util.Scanner;


public class PrintNumberInWordV2 {

    public static void main(String[] args) {


        System.out.println("Enter a number between 1 and 9: ");

        Scanner in = new Scanner(System.in);
        int x = in.nextInt();




        useNestedIf(x);

    }

    private static void useNestedIf(int x) {
        String numberStr = null;
        if (0 == x) {
            numberStr = "ZERO";
        } else if (1 == x) {
            numberStr = "ONE";
        } else if (2 == x) {
            numberStr = "TWO";
        } else if (3 == x) {
            numberStr = "THREE";
        } else if (4 == x) {
            numberStr = "FOUR";
        } else if (5 == x) {
            numberStr = "FIVE";
        } else if (6 == x) {
            numberStr = "SEX";
        } else if (7 == x) {
            numberStr = "SEVEN";
        } else if (8 == x) {
            numberStr = "EIGHT";
        } else if (9 == x) {
            numberStr = "NINE";
        } else {
            numberStr = "OTHER";
        }
        System.out.println("(a) Use a \"nested-if\" statement: " + numberStr);
    }
}
