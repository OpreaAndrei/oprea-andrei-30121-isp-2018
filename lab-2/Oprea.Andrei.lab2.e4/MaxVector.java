package Oprea.Andrei.Lab2.ex4;

import java.util.Scanner;

public class MaxVector {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Introduceti dimensiunea vectorului");
        int n = in.nextInt();
        int[] v = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.println("introduceti elementul " + i + ": ");
            v[i] = in.nextInt();
            System.out.println();
        }
        int max = v[0];

        for (int i = 1; i < n; i++) {
            if (v[i] > max)
            max = v[i];
        }
        System.out.println("Nr maxim din vector este " + max);
    }


}
