package Oprea.Andrei.Lab3.ex1;

public class Sensor {
    int value;

    Sensor() {
        this.value = -1;
    }

    public void add(int k) {
        value += k;
    }

    public int getValue() {
        return this.value;
    }



}
