package Oprea.Andrei.Lab3.ex3;

public class Author {
    private String name, email;
    private char gender;


    public Author(String name, String email, char gender) {

        this.email = email;
        this.gender = gender;
        this.name = name;
    }

    public String getName() {
        return name;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public char getGender() {
        if(Character.toLowerCase(gender)=='m' || Character.toLowerCase(gender)=='f')
        return gender;
        else
        {
            System.out.println("gender is not accepted");
            return 0;
        }

    }

    public void tooString()
    {
        System.out.println(this.name+" ("+ this.gender+") at " +this.email);

    }
}
