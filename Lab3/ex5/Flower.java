package Oprea.Andrei.Lab3.ex5;

public class Flower {

    int petal;
    static int value=0;

    Flower() {
        System.out.println("Flower has been created!");
        value++;
    }
    public static int value()
    {
        return value;
    }

    public static void main(String[] args) {
        Flower[] garden = new Flower[5];
        for (int i = 0; i < 5; i++) {
            Flower f = new Flower();
            garden[i] = f;
        }
        System.out.println("Number of objects created is "+value());
    }
}

