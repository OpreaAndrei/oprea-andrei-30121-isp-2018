package Oprea.Andrei.Lab3.ex4;

public class MyPoint {
    private int x, y;

    public MyPoint() {
        this.x = 0;
        this.y = 0;
    }

    public MyPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setXY(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void tooString() {
        System.out.println("(" + this.x + "," + this.y + ")");
    }

    public double distance(int x, int y) {
        double a;
        a = Math.sqrt((this.x - x) * (this.x - x) + (this.y - y) * (this.y - y));
        return a;
    }

    public double distance(MyPoint a) {
        double b;
        b = Math.sqrt((this.x - a.x) * (this.x - a.x) + (this.y - a.y) * (this.y - a.y));
        return b;
    }
}

