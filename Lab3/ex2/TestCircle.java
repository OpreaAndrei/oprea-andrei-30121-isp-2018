package Oprea.Andrei.Lab3.ex2;

public class TestCircle {
    public static void main(String[] args) {
        Circle c1 = new Circle();
        Circle c2 = new Circle(2.5, "blue");


        System.out.println(c1.getColor());
        System.out.println(c2.getRadius());
        System.out.println("Aria:" + c2.getArea());
    }
}
