package Oprea.Andrei.Lab4.ex1;

public class Circle {
    private double radius = 1.0;
    private String color = "red";
    private double area;

    public Circle() {
        this.radius = 2.0;
        this.color = "blue";

    }

    public Circle(double radius, String color) {
        this.radius = radius;
        this.color = color;

    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        area = radius * radius * 3.14;
        return area;
    }
}
