package Oprea.Andrei.Lab4.ex6;

public class Square extends Rectangle {
    public Square() {
    }

    public Square(double side) {
        super(side, side);
    }

    public Square(double side, String color, boolean isFilled) {
        super(side, side, color, isFilled);
    }

    public double getSide() {
        return this.getLenght();
    }

    public void setSide(double side) {
        this.setLenght(side);
        this.setWith(side);
    }

    @Override
    public void setWith(double x) {
        super.setWith(x);
        super.setLenght(x);
    }

    @Override
    public void setLenght(double y) {
        super.setLenght(y);
        super.setWith(y);
    }

}
