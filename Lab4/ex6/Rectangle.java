package Oprea.Andrei.Lab4.ex6;

public class Rectangle extends Shape {
    private double with;
    private double lenght;

    public Rectangle() {
        this.with = 1.0;
        this.lenght = 1.0;
    }

    public Rectangle(double with, double lenght) {
        this.with = with;
        this.lenght = lenght;
    }

    public Rectangle(double with, double lenght, String color, boolean isFilled) {
        super(color, isFilled);
        this.with = with;
        this.lenght = lenght;
    }

    public double getWith() {
        return with;
    }

    public void setWith(double with) {
        this.with = with;
    }

    public double getLenght() {
        return lenght;
    }

    public void setLenght(double lenght) {
        this.lenght = lenght;
    }

    public double getAria() {
        return this.with * this.lenght;
    }

    public double getPerimeter() {
        return 2 * (this.with + this.lenght);
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "with=" + this.getWith() +
                ", lenght=" + this.getLenght() +
                '}';
    }
}
