package Oprea.Andrei.Lab4.ex3;

import Oprea.Andrei.Lab4.ex2.Author;


public class Book {
    private String name;
    private double price;
    private int qtyInStock;
    private Author author;

    public Book(String name, Author author, double price) {

    }

    public Book(String name, Author author, double price, int qtyInStock) {

    }

    public Book(String sal, String gigi, int price, int qtyInStock) {
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public Author getAuthor() {
        return author;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    @Override
    public String toString() {
        return  name + "by " + author + "(" + author.getGender() + ") at " + author.getEmail();
    }


}
