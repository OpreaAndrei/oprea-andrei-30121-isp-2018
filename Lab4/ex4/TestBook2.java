package Oprea.Andrei.Lab4.ex4;
import Oprea.Andrei.Lab4.ex2.*;

public class TestBook2 {
    public static void main(String[] args) {
        Author[] autori = new Author[2];
        autori[0]= new Author("Oprea","gmail",'m');
        autori[1]= new Author("Razvan","gmail",'m');

        Book b1 = new Book("prima carte",autori,20.5,10);
        b1.printAuthors();
        System.out.println(b1.toString());
    }
}
