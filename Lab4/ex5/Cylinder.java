package Oprea.Andrei.Lab4.ex5;

import Oprea.Andrei.Lab3.ex2.Circle;

public class Cylinder extends Circle {

    private double height;


    public Cylinder() {
        this.height = 1.0;
    }

    public Cylinder(double radius) {
        super(radius);
    }

    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    public double getHeight() {
        return this.height;
    }


    public double getAria() {
        return this.getRadius() * this.getRadius() * 3.14 * this.height;
    }

}

