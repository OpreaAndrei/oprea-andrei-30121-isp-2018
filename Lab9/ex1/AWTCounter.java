package Oprea.Andrei.Lab9.ex1;

import java.awt.*;         // Using AWT's containers and components
import java.awt.event.*;   // Using AWT's event classes and listener interfaces

// An AWT GUI program inherits the top-level container java.awt.Frame
public class AWTCounter extends Frame implements ActionListener {
    private Label lblCount;     // Declare component Label
    private TextField tfCount;  // Declare component TextField
    private Button btnCount1,btnCount2;    // Declare component Button
    private int count = 0;      // counter's value

    // Constructor to setup UI components and event handlers
    public AWTCounter () {
        setLayout(new FlowLayout());


         lblCount = new Label("Counter"); // Construct component Label
        add(lblCount);                  // "super" Frame adds Label

        tfCount = new TextField(count + "", 10); // Construct component TextField
        tfCount.setEditable(false);       // read-only
        add(tfCount);                     // "super" Frame adds TextField

        btnCount1 = new Button("Count++");   // Construct component Button
        add(btnCount1);                    // "super" Frame adds Button
        btnCount1.addActionListener(this::actionPerformed);

        btnCount2 = new Button("Count--");   // Construct component Button
        add(btnCount2);                    // "super" Frame adds Button
        btnCount2.addActionListener(this::actionPerformed2);

        setSize(250, 100);       // "super" Frame sets initial size
        setTitle("AWT Counter"); // "super" Frame sets title
        setVisible(true);        // show "super" Frame
    }

    // ActionEvent handler - Called back when the button is clicked.

    @Override
    public void actionPerformed(ActionEvent evt) {
        ++count;                     // Incrase the counter value
        tfCount.setText(count + ""); // Display on the TextField
        // setText() takes a String
    }

    public void actionPerformed2(ActionEvent evt){
        --count;
        tfCount.setText(count + "");

    }

    // The entry main() method
    public static void main(String[] args) {
        // Invoke the constructor by allocating an anonymous instance
        new AWTCounter();
    }
}

