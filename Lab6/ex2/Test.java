package Oprea.Andrei.Lab6.ex2;


import java.util.List;
import java.util.stream.Collectors;

public class Test {
    public static void main(String[] args) {
        Bank bank = new Bank();
        bank.addAccount("Andrei",100);
        bank.addAccount("Robert",200);
        bank.addAccount("Maria",300);
        bank.addAccount("Ionut",500);
        bank.addAccount("Alexandru",10);

        List accounts_2 = (List) bank.getAllAccounts()
                .stream()
                .sorted((b1,b2)->((BankAccount)b1).getOwner().compareTo(((BankAccount)b2).getOwner()))
                .collect(Collectors.toList());

        accounts_2.forEach(b->System.out.println("Owner: "+((BankAccount)b).getOwner()+" Balance: "+((BankAccount)b).getBalance()));

    }

}
