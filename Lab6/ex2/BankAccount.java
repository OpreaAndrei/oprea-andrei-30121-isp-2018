package Oprea.Andrei.Lab6.ex2;

import java.util.Objects;

public class BankAccount {
    public String owner;
    public double balance;

    public BankAccount(String owner, double balance) {
        this.balance = balance;
        this.owner = owner;
    }

    public String getOwner() {
        return owner;
    }

    public double getBalance() {
        return balance;
    }

    public void withdraw(double amount) {
        if (this.balance <= amount) {
            System.out.println("Insuficient founds");

        } else this.balance -= amount;

    }

    public void deposit(double amount) {
        this.balance += amount;
    }

    public boolean equals(Object obj) {
        if (obj instanceof BankAccount) {
            BankAccount ba = (BankAccount) obj;
            return balance == ba.balance && ba.owner.equals(owner);
        }
        return false;
    }

    public int hashCode() {
        return (int) balance + owner.hashCode();
    }


}

