package Oprea.Andrei.Lab6.ex3;


import java.util.Scanner;

public class ConsoleApplication {
    public static void main(String[] args) {
        Dictionary d = new Dictionary();
        Scanner scan = new Scanner(System.in);
        int c = 0;

        while (c <= 4) {


            System.out.println("Meniu\n");
            System.out.println("1 - Add word");
            System.out.println("2 - Get definition");
            System.out.println("3 - Get all words");
            System.out.println("4 - Get all definitions");
            System.out.println("5- Exit");
            System.out.println("Enter choice");
            c = scan.nextInt();

            switch (c) {
                case 1: {
                    System.out.println("Add word");
                    Word w = new Word(scan.next());
                    System.out.println("Add definition");
                    Definition d1 = new Definition(scan.next());
                    d.addWord(w, d1);
                    break;

                }
                case 2: {
                    System.out.println("Get definition");
                    Word w = new Word(scan.next());
                    if(d.getDefinition(w)==null) {
                        System.out.println("nu exista");
                    }
                    else  System.out.println("Definition is: " + d.getDefinition(w).getDescription());
                    break;
                }
                case 3: {
                    System.out.println("Getting all words");

                    for (Word w : d.getAllWords()){
                        System.out.println(w.getName());
                    }
                    break;
                }
                case 4: {
                    System.out.println("Getting all definitions");
                    for (Definition dd : d.getAllDefinitions()){
                        System.out.println(dd.getDescription());
                    }
                    break;
                }
                default:
                    break;
            }

        }
    }
}
