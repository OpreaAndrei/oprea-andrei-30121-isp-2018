package Oprea.Andrei.Lab6.ex3;

import java.util.Objects;

public class Word {
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Word word = (Word) o;
        return Objects.equals(name, word.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    public String getName() {
        return name;
    }

    private String name;

    public Word(String name) {
        this.name = name;
    }
}
