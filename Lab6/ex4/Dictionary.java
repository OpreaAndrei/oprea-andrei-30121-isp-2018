package Oprea.Andrei.Lab6.ex3;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

public class Dictionary {

    private HashMap<Word, Definition> dictionary = new HashMap<>();

    public void addWord(Word w, Definition d) {
        dictionary.put(w,d);
    }

    public Definition getDefinition(Word w) {
        return dictionary.get(w);
    }

    public Collection<Word> getAllWords() {
       return dictionary.keySet();
           }

    public Collection<Definition> getAllDefinitions() {
        return  dictionary.values();
    }
}

