package Oprea.Andrei.Lab6.ex3;

public class Definition {
    private String description;

    public Definition(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
