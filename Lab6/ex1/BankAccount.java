package Oprea.Andrei.Lab6.ex1;

import java.util.Objects;

public class BankAccount {
    private String Owner;
    private double balance;

    public void withdraw(double amount) {
        if (this.balance <= amount) {
            System.out.println("Insuficient founds");

        } else this.balance -= amount;

    }

    public void deposit(double amount) {
        this.balance += amount;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof BankAccount)) return false;

        BankAccount b = (BankAccount) obj;
        return Objects.equals(this.balance, b.balance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Owner, balance);
    }



}
