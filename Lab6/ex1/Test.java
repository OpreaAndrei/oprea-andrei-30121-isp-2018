package Oprea.Andrei.Lab6.ex1;

import java.sql.SQLOutput;

public class Test {
    public static void main(String[] args) {
        BankAccount b1 = new BankAccount();
        BankAccount b2 = new BankAccount();

        b1.deposit(100);
        b2.deposit(100);

        System.out.println(b1.equals(b2));
        System.out.println(b1.hashCode());
    }
}
