package Oprea.Andrei.Lab7.ex4;

import java.io.Serializable;

public class Car implements Serializable {

    String model;
    int pret;

    public Car(String model, int pret) {
        this.model = model;
        this.pret = pret;
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", pret=" + pret +
                '}';
    }

    public String getModel() {
        return model;
    }

    public int getPret() {
        return pret;
    }
}
