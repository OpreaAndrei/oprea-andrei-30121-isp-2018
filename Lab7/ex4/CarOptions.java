package Oprea.Andrei.Lab7.ex4;

import java.io.*;

public class CarOptions {

    public void saveCar (Car car,String saveLocation) throws IOException
    {
        ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream(saveLocation));
        o.writeObject(car);
        System.out.println("The car: " +car.getModel()+ " was saved here: "+saveLocation);
    }
    public Car retriveCar(String saveLocation) throws IOException, ClassNotFoundException {
        ObjectInputStream o = new ObjectInputStream(new FileInputStream(saveLocation));

        Car car = (Car) o.readObject();
        return car;
    }

}
