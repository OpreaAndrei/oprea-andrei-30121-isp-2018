package Oprea.Andrei.Lab7.ex4;

import java.io.IOException;

public class Test {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Car c1 = new Car("tesla",1000);
        Car c2 = new Car("bmw",2000);

        CarOptions o1= new CarOptions();
        o1.saveCar(c1,"car1.dat");
        o1.saveCar(c2,"car2.dat");

        Car a = o1.retriveCar("car1.dat");
        Car b = o1.retriveCar("car2.dat");

        System.out.println(a.toString());
        System.out.println(b.toString());
        
    }
}
