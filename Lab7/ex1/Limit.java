package Oprea.Andrei.Lab7.ex1;

public class Limit extends Exception {
    private int n;
    public Limit(String m,int n){
        super(m);
        this.n=n;
    }

    public int getN() {
        return n;
    }
}
