package Oprea.Andrei.Lab7.ex3;

import java.io.*;
import java.util.Scanner;

public class Encrypter {
    public static void main(String[] args) throws IOException {

        System.out.println("Enter path");
        Scanner scan = new Scanner(System.in);
        String path = scan.next();
        System.out.println("1.Encrypt \n2.Decrypt");
        int choice = scan.nextInt();
        System.out.println(path);
        Operation(path, choice);
    }


    public static void Operation(String path, int choice) throws IOException {
        path=".\\src\\Oprea\\Andrei\\Lab7\\ex3\\"+path;
        BufferedReader in = new BufferedReader(new FileReader(path));
        String s, s2 = new String();
        String path2 = new String();
        while ((s = in.readLine()) != null) {
            for (char l : s.toCharArray()) {
                if (choice == 1) {
                    s2 += (char) ((int) l << 1);
                    path2 = ".enc";
                } else{ s2 += (char) ((int) l >> 1);
                path2 = ".dec";}
            }
            s2 += "\n";
        }
        in.close();

        BufferedReader out = new BufferedReader(new StringReader(s2));

        PrintWriter out1 = new PrintWriter(new BufferedWriter(new FileWriter(".\\src\\Oprea\\Andrei\\Lab7\\ex3\\data" + path2)));

        while ((s2 = out.readLine()) != null)
            out1.println(s2);
        out1.close();

    }
}
