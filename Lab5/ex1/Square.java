package Oprea.Andrei.Lab5.ex1;

public class Square extends Rectangle {
    public Square() {

    }

    public Square(double side) {
        this.setLength(side);
        this.setWidth(side);


    }

    public Square(double side, String color, boolean filled) {
        this.setLength(side);
        this.setWidth(side);
        this.color = color;
        this.filled = filled;
    }
    public double getSide(){
        return this.getLength();
    }

    public void setSide(double side){
        this.setLength(side);
        this.setWidth(side);
    }

    @Override
    public void setWidth(double side){
        super.setWidth(side);
    }

    @Override
    public void setLength(double side){
        super.setLength(side);
    }

    @Override
    public String toString(){
        return "Square";
    }


}
