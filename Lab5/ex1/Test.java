package Oprea.Andrei.Lab5.ex1;

public class Test {
    public static void main(String[] args) {


        Circle c1 = new Circle();
        Circle c2 = new Circle(2, "blue", true);
        Rectangle r1 = new Rectangle();
        Rectangle r2 = new Rectangle(2, 3, "yellow", false);
        Square s1 = new Square();
        Square s2 = new Square(1, "pink", true);

        System.out.println(c2.getArea());
        System.out.println(c2.getPerimeter());

        System.out.println(r2.getArea());
        System.out.println(r2.getPerimeter());

        System.out.println(s2.getSide());
        System.out.println(s2.getPerimeter());



    }


}
