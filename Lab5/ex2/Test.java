package Oprea.Andrei.Lab5.ex2;

public class Test {
    public static void main(String[] args) {

        ProxyImage Proxy = new ProxyImage("Rotated");
        ProxyImage Proxy2 = new ProxyImage("Real");

        Proxy.display();
        Proxy2.display();

    }
}
