package Oprea.Andrei.Lab5.ex2;

import java.sql.SQLOutput;

public class ProxyImage implements Image {

    private Image image;
    private String fileName;


    public ProxyImage(String fileName) {
        this.fileName = fileName;
    }


    @Override
    public void display() {
        if (fileName == "Real") {
            image = new RealImage(fileName);
        } else if (fileName == "Rotated") {
            image = new RotatedImage(fileName);
        }
        image.display();
    }
}



