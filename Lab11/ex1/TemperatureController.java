package Oprea.Andrei.Lab11.ex1;

public class TemperatureController {
    TemperatureSensor t;
    TemperatureTextView tview;
    public TemperatureController(TemperatureSensor t, TemperatureTextView tview){
        t.addObserver(tview);
        this.t = t;
        this.tview = tview;

    }

}
